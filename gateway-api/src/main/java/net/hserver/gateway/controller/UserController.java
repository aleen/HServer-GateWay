package net.hserver.gateway.controller;

import top.hserver.core.ioc.annotation.Controller;
import top.hserver.core.ioc.annotation.GET;
import top.hserver.core.ioc.annotation.POST;
import top.hserver.core.server.util.JsonResult;
import net.hserver.gateway.utils.UUIDUtil;

import java.util.ArrayList;
import java.util.List;

@Controller("/v1/user/")
public class UserController {

    @POST("login")
    public JsonResult login(String userName, String password) {
        if ("admin".equals(userName) && "admin".equals(password)) {
            return JsonResult.ok().put("token", UUIDUtil.randomUUID32());
        } else {
            return JsonResult.error();
        }
    }


    @GET("getInfo")
    public JsonResult getInfo(String token) {
        if (token != null) {
            List<String> list = new ArrayList<>();
            list.add("super_admin");
            list.add("admin");
            return JsonResult.ok().put("access", list);
        }
        return JsonResult.error();
    }

}
