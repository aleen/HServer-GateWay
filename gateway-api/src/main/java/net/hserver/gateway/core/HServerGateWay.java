package net.hserver.gateway.core;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import top.hserver.core.server.util.NamedThreadFactory;


/**
 * @author hxm
 */
@Slf4j
public class HServerGateWay extends Thread {

    @Override
    public void run() {
        EventLoopGroup bossGroup = null;
        EventLoopGroup workerGroup = null;
        int acceptThreadCount = 2;
        int ioThreadCount = 4;
        try {
            ServerBootstrap b = new ServerBootstrap();
            bossGroup = new NioEventLoopGroup(acceptThreadCount, new NamedThreadFactory("hserver-gateway_boss@"));
            workerGroup = new NioEventLoopGroup(ioThreadCount, new NamedThreadFactory("hserver-gateway_ worker@"));
            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class);
            b.childHandler(new GateWayServerInitializer());
            Channel ch = b.bind(80).sync().channel();
            log.info("网关启动完成，端口:{}", 80);
            ch.closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
