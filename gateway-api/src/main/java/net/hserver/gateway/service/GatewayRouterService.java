package net.hserver.gateway.service;

import org.beetl.sql.core.page.PageResult;
import net.hserver.gateway.dto.GatewayRouterDTO;
import net.hserver.gateway.dto.GatewayRouterNodeDTO;

import java.util.List;

/**
 * @author hxm
 */
public interface GatewayRouterService {

  String add(GatewayRouterDTO gatewayRouterDTO);
  boolean update(GatewayRouterDTO gatewayRouterDTO);
  boolean remove(String id);
  PageResult list(Long page, Long pageSize);
  List<GatewayRouterNodeDTO> all();
  List<GatewayRouterNodeDTO> getOne(String id);
}
