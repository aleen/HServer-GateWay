import axios from '@/libs/api.request'

export const allNode = (params) => {
  return axios.request({
    url: '/v1/serverNodeStatus/all',
    method: 'get',
  })
};
