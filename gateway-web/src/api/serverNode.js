import axios from '@/libs/api.request'

export const addNode = (data) => {
  return axios.request({
    url: '/v1/serverNode/add',
    method: 'post',
    data: data,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

export const listNode = (data) => {
  return axios.request({
    url: '/v1/serverNode/list'+data,
    method: 'get',
  })
}

export const updateNode = (data) => {
  return axios.request({
    url: '/v1/serverNode/update',
    method: 'post',
    data: data,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
};

export const removeNode = (params) => {
  return axios.request({
    url: '/v1/serverNode/remove' + params,
    method: 'get',
  })
};

export const allNode = (params) => {
  return axios.request({
    url: '/v1/serverNode/all',
    method: 'get',
  })
};
