import axios from '@/libs/api.request'

export const addRouter = (data) => {
  return axios.request({
    url: '/v1/router/add',
    method: 'post',
    data: data,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}


export const listRouter = (data) => {
  return axios.request({
    url: '/v1/router/list'+data,
    method: 'get',
  })
}


export const updateRouter = (data) => {
  return axios.request({
    url: '/v1/router/update',
    method: 'post',
    data: data,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
};


export const removeRouter = (params) => {
  return axios.request({
    url: '/v1/router/remove' + params,
    method: 'get',
  })
};
