package net.hserver.gateway.dao;

import net.hserver.gateway.entity.GateWayRouter;
import net.hserver.plugins.beetlsql.annotation.BeetlSQL;
import org.beetl.sql.mapper.BaseMapper;

/**
 * @author hxm
 */
@BeetlSQL
public interface GatewayRouterDao extends BaseMapper<GateWayRouter> {

}
