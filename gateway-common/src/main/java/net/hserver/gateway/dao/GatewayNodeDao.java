package net.hserver.gateway.dao;

import net.hserver.plugins.beetlsql.annotation.BeetlSQL;
import org.beetl.sql.mapper.BaseMapper;
import net.hserver.gateway.entity.GateWayNode;

/**
 * @author hxm
 */
@BeetlSQL
public interface GatewayNodeDao extends BaseMapper<GateWayNode> {

}
