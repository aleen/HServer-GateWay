package net.hserver.gateway.dto;

import lombok.Data;

/**
 * @author hxm
 */
@Data
public class GatewayRouterNodeDTO {

  /**
   * 上线还是备用  true 是备用的
   */
  private Boolean type;

  /**
   * 节点ID
   */
  private String nodeId;

  /**
   * 路由ID
   */
  private String routerId;

  /**
   * url
   */
  private String url;

  /**
   * ip
   */
  private String ip;


  /**
   * 点开
   */
  private Integer port;


  /**
   * 节点名字
   */
  private String nodeDesc;

  /**
   *  路由名字
   */
  private String routerDesc;

  /**
   * 权重
   */
  private int weight;

  /**
   * 负载类型
   */
  private Integer strategyType;
}
