package net.hserver.gateway.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * @author hxm
 */
@Table(name = "gateway_router")
@Data
public class GateWayRouter {
    @AssignID
    private String id;
    private String url;
    private String nodeIds;
    private String desc;
    /**
     * 域名
     */
    private String domain;
    /**
     * 负载算法类型
     */
    private Integer strategyType;

}
