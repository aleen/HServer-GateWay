package net.hserver.gateway.utils;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author hxm
 */
public class JvmStackUtil {

    private static final long MB = 1048576L;

    public static HashMap<String, String> getMemoryInfo() {
        MemoryMXBean memory = ManagementFactory.getMemoryMXBean();
        MemoryUsage headMemory = memory.getHeapMemoryUsage();
        HashMap<String, String> data = new HashMap<>();
        data.put("init", headMemory.getInit() / MB + "MB");
        data.put("max", headMemory.getMax() / MB + "MB");
        data.put("committed",headMemory.getCommitted() / MB + "MB");
        data.put("used", headMemory.getUsed() * 100 / headMemory.getCommitted() + "%");
        return data;

    }


}