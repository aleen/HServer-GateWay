package net.hserver.gateway.core;

import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.*;
import io.netty.util.ReferenceCountUtil;
import net.hserver.gateway.core.bean.Proxy;
import top.hserver.core.ioc.annotation.Bean;
import top.hserver.core.server.util.ExceptionUtil;
import net.hserver.gateway.core.exception.GateWayException;

import java.nio.charset.StandardCharsets;

/**
 * 网关异常输出输出
 *
 * @author hxm
 */
@Bean
public class GateWayOut {

    public void out(GateWayException e, Proxy proxy) {
        StringBuilder html = new StringBuilder();
        html.append("<!DOCTYPE html>");
        html.append("<html lang=\"zh-CN\">");
        html.append("<head>");
        html.append("  <meta charset=\"utf-8\">");
        html.append("<title>错误提示-HServerGateWay</title>");
        html.append("  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">");
        html.append("  <style>");
        html.append("    body{");
        html.append("      font: 16px arial,'Microsoft Yahei','Hiragino Sans GB',sans-serif;");
        html.append("    }");
        html.append("    h1{");
        html.append("      margin: 0;");
        html.append("      color:#3a87ad;");
        html.append("      font-size: 26px;");
        html.append("    }");
        html.append("    .content{");
        html.append("      width: 45%;            ");
        html.append("      margin: 0 auto;        ");
        html.append("                             ");
        html.append("    }                        ");
        html.append("    .content >div{           ");
        html.append("      margin-top: 200px;     ");
        html.append("      padding: 20px;         ");
        html.append("      background: #d9edf7;   ");
        html.append("      border-radius: 12px;   ");
        html.append("    }                        ");
        html.append("    .content dl{             ");
        html.append("      color: #2d6a88;        ");
        html.append("      line-height: 40px;     ");
        html.append("    }                        ");
        html.append("    .content div div {       ");
        html.append("      padding-bottom: 20px;  ");
        html.append("      text-align:center;     ");
        html.append("    }                        ");
        html.append("  </style>                   ");
        html.append("</head>      ");
        html.append("<body>       ");
        html.append("  <div class=\"content\"> ");
        html.append("      <div>  ");
        html.append("           <h1> ");
        html.append(e.getStatus());
        html.append("</h1>");
        html.append("        <dl> ");
        html.append("          <dt>描述:</dt>");
        html.append("          <dd>");
        html.append(e.getMessage());
        html.append("  </dd>");
        html.append("             ");
        html.append("        </dl>");

        if (e.getThrowable()!=null) {
            html.append("        <dl> ");
            html.append("          <dt>原因:</dt>");
            html.append("          <dd>");
            html.append(ExceptionUtil.getHtmlMessage(e.getThrowable()));
            html.append("  </dd>");
            html.append("             ");
            html.append("        </dl>");
        }
        html.append("      </div>");
        html.append("    </div> ");
        html.append("</body>");
        html.append("</html>");
        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                e.getStatus(),
                Unpooled.wrappedBuffer(html.toString().getBytes(StandardCharsets.UTF_8)));
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html;charset=UTF-8");
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
        response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        proxy.getCtx().channel().writeAndFlush(response);
        if (proxy.getMsg() != null) {
            ReferenceCountUtil.release(proxy.getMsg());
        }
    }

}
