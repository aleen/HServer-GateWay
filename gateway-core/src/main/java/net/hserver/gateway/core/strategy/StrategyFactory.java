package net.hserver.gateway.core.strategy;

import net.hserver.gateway.core.strategy.imp.*;

import java.util.List;

/**
 * @author hxm
 */
public class StrategyFactory{

    public static <T> Chose<T> getProduct(List<T> list,Class claszz) {
      if (claszz==PollingStrategy.class){
        return PollingStrategy.getInstance(list);
      }else if (claszz== RandomStrategy.class){
        return RandomStrategy.getInstance(list);
      }else if (claszz== WeightPollingStrategy.class){
          return WeightPollingStrategy.getInstance(list);
      }else if (claszz== HashConsistencyStrategy.class){
          return HashConsistencyStrategy.getInstance(list);
      }else if (claszz== MinimumConnectionStrategy.class){
          return MinimumConnectionStrategy.getInstance(list);
      }
      return null;
    }
}
