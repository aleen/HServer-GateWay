package net.hserver.gateway.core.strategy;


import java.util.List;

/**
 * @author hxm
 */
public interface Chose<T> {

  /**
   *选择一个host
   *
   * @return
   */
  T choseInfo(String ip);
}
