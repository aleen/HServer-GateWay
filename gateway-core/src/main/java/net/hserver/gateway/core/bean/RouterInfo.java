package net.hserver.gateway.core.bean;

import lombok.Data;
import net.hserver.gateway.config.StrategyType;

/**
 * @author hxm
 */
@Data
public class RouterInfo {

  public RouterInfo() {
  }

  public RouterInfo(String routerId, StrategyType strategyType) {
    this.routerId = routerId;
    this.strategyType = strategyType;
  }

  private String routerId;

  private StrategyType strategyType;

}
